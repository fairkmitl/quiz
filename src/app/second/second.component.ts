import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { HttpClient } from '@angular/common/http';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

/**
 * @title Table with filtering
 */
@Component({
  // selector: 'second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.scss'],
})
export class SecondComponent implements OnInit {
  displayedColumns: string[] = ['title', 'subtitle'];
  song = [];
  dataSource = new MatTableDataSource(this.song);

  constructor(private http: HttpClient) {}

  loading = false;

  ngOnInit(): void {
    this.loading = true;
    this.fetchData();
  }

  fetchData() {
    let url = 'https://shazam.p.rapidapi.com/charts/track';

    this.http
      .get<any>(url, {
        params: { locale: 'en-US', pageSize: '10', startFrom: '0' },
        headers: {
          'x-rapidapi-key':
            'ff3c6ad228msh37d5d1150b04db0p1dbda1jsnf85d6e8aefa7',
          'x-rapidapi-host': 'shazam.p.rapidapi.com',
        },
      })
      .subscribe((data) => {
        this.dataSource = new MatTableDataSource(data.tracks);
        this.loading = false;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
