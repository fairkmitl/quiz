import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './question1.component.html',
  styleUrls: ['./question1.component.scss'],
})
export class Question1Component implements OnInit {
  title = 'question1';

  ngOnInit(): void {
    console.log('question1');
  }
}
